package OpenFDM
  package Aerodynamics
    model Simple
      import SI = Modelica.SIunits;
      extends Modelica.Mechanics.MultiBody.Interfaces.PartialOneFrame_b;
      parameter Real cLAlpha = 1.0 / 20.0 / 57.0 "Lift curve slope";
      parameter Real cD0 = 0.001 "Drag coefficient at cL=0";
      parameter Real cL0 = 0.1 "Lift coefficient at alpha=0";
      parameter Real cDcLSq = 0.001 "Drag coefficient due to cL^2";
      parameter Real cmalpha = -0.001 "Slope of pitch moment due to alpha";
      parameter Real clp = -0.1 "Roll moment due to roll rate";
      parameter Real cmq = -0.1 "Pitch moment due to pitch rate";
      parameter Real cnr = -0.1 "Yaw moment due to yaw rate";
      parameter SI.Area s = 0.1 "Wing area";
      parameter SI.Area b = 1.0 "Wing span";
      parameter SI.Area cbar = 0.1 "Wing average chord";
      SI.Force lift "Lift force";
      SI.Force drag "Drag force";
      SI.Force side "Side force";
      Real cL "Lift coefficient";
      Real cD "Drag coefficient";
      Real cC "Side coefficient";
      Real cl "Roll moment coefficient";
      Real cm "Pitch moment coefficient";
      Real cn "Yaw moment coefficient";
      SI.Torque L "Rolling moment";
      SI.Torque M "Pitching moment";
      SI.Torque N "Yawing moment";
      Modelica.Mechanics.MultiBody.Forces.WorldForceAndTorque forceAndTorque(resolveInFrame = Modelica.Mechanics.MultiBody.Types.ResolveInFrameB.frame_resolve) annotation(Placement(visible = true, transformation(origin = {-40.6181,34.4371}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      OpenFDM.Aerodynamics.Atmosphere atmosphere annotation(Placement(visible = true, transformation(origin = {-5.29806,-43.2671}, extent = {{-10,-10},{10,10}}, rotation = 0)));
    equation
      connect(forceAndTorque.frame_b,frame_b) annotation(Line(points = {{-30.6181,34.4371},{-10.1545,34.4371},{-10.1545,0.441501},{-10.1545,0.441501},{-10.1545,0.441501}}));
      connect(atmosphere.frame_b,frame_b) annotation(Line(points = {{-15.2981,-43.2671},{-29.5806,-43.2671},{-29.5806,-0.441501},{-11.0375,-0.441501},{-11.0375,-0.441501}}));
      connect(forceAndTorque.frame_b,forceAndTorque.frame_resolve) annotation(Line(points = {{-30.6181,34.4371},{-24.7241,34.4371},{-24.7241,50.3311},{-39.7351,50.3311},{-39.7351,44.4371},{-40.6181,44.4371}}));
      cL = cLAlpha * atmosphere.alpha + cL0;
      cD = cDcLSq * cL * cL + cD0;
      cC = 0;
      cl = b / (2 * atmosphere.airspeedTrue) * clp * atmosphere.rollRate;
      cm = cmalpha * atmosphere.alpha + cbar / (2 * atmosphere.airspeedTrue) * cmq * atmosphere.pitchRate;
      cn = b / (2 * atmosphere.airspeedTrue) * cnr * atmosphere.yawRate;
      lift = cL * atmosphere.q * s;
      drag = cD * atmosphere.q * s;
      side = cC * atmosphere.q * s;
      L = cl * atmosphere.q * s * b;
      M = cm * atmosphere.q * s * cbar;
      N = cn * atmosphere.q * s * b;
      forceAndTorque.force = {-drag,side,-lift};
      forceAndTorque.torque = {L,M,N};
      annotation(Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = false, initialScale = 0.1, grid = {2,2}), graphics = {Text(origin = {-1.32413,-9.27391}, extent = {{-93.6,91.83},{96.2473,14.1334}}, textString = "simple"),Text(origin = {-15.0065,-24.72}, extent = {{-79.91,30.46},{105.522,-39.736}}, textString = "aero"),Rectangle(origin = {-0.441501,0}, extent = {{-95.3642,95.3642},{95.3642,-95.3642}})}), experiment(StartTime = 0, StopTime = 7000, Tolerance = 0.000001));
    end Simple;
    model Atmosphere
      import SI = Modelica.SIunits;
      //
      // Interfaces
      //
      extends Modelica.Mechanics.MultiBody.Interfaces.PartialOneFrame_b;
      //
      // VariablesRel
      //
      SI.Distance asl "altitude above sea level";
      SI.Angle latitude "latitude";
      SI.Angle longitude "longitude";
      SI.Distance hGeo "geopotential altitude";
      Real Temperature,Pressure;
      SI.Angle alpha "angle of attack";
      SI.Angle beta "angle of side-slip";
      SI.Velocity airspeedTrue "true airspeed";
      SI.Pressure rho = 1.225 "atmoshphere density";
      SI.Pressure q "dynamics pressure";
      SI.AngularVelocity rollRate "roll rate";
      SI.AngularVelocity pitchRate "pitch rate";
      SI.AngularVelocity yawRate "yaw rate";
      //
      // Models
      //
      outer OpenFDM.World.AeroWorld aeroworld annotation(Placement(visible = true, transformation(origin = {-70.1987,72.4062}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      Modelica.Blocks.Tables.CombiTable1Ds atmosphereTable(table = {{0,288.15,1.0},{11000,216.65,0.2233611},{20000,216.66,0.05403295},{32000,228.65,0.0085666784},{47000,270.65,0.0010945601},{51000,270.65,0.00066063531},{71000,214.65,0.000039046834},{84852,189.946,0.00000368501}}) annotation(Placement(visible = true, transformation(origin = {16.3355,-67.9911}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      Modelica.Mechanics.MultiBody.Sensors.RelativeSensor relativesensor(resolveInFrame = Modelica.Mechanics.MultiBody.Types.ResolveInFrameAB.frame_a, sequence = {3,2,1}, get_v_rel = true, get_w_rel = true, resolveInFrameAfterDifferentiation = Modelica.Mechanics.MultiBody.Types.ResolveInFrameAB.frame_b, animation = false) annotation(Placement(visible = true, transformation(origin = {-31.3466,-3.53201}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      OpenFDM.Frames.WCWF wcwf annotation(Placement(visible = true, transformation(origin = {-80.7947,-2.20751}, extent = {{-10,-10},{10,10}}, rotation = 0)));
    equation
      connect(relativesensor.frame_resolve,relativesensor.frame_b) annotation(Line(points = {{-21.3466,4.46799},{-21.6336,4.46799},{-21.6336,-3.53201},{-21.6336,-3.53201}}));
      connect(relativesensor.frame_b,frame_b) annotation(Line(points = {{-21.3466,-3.53201},{9.27152,-3.53201},{9.27152,1.3245},{9.27152,1.3245}}));
      connect(wcwf.frame_b,relativesensor.frame_a) annotation(Line(points = {{-70.7947,-2.20751},{-40.6181,-2.20751},{-40.6181,-4.41501},{-40.6181,-4.41501}}));
      (latitude,longitude,asl) = aeroworld.wcwfToGeodetic(frame_b.r_0);
      (alpha,beta,airspeedTrue) = OpenFDM.Util.cartesianToSpherical(relativesensor.v_rel);
      hGeo = asl * aeroworld.a / (asl + aeroworld.a);
      q = 0.5 * rho * airspeedTrue ^ 2;
      {rollRate,pitchRate,yawRate} = Modelica.Mechanics.MultiBody.Frames.angularVelocity2(frame_b.R);
      connect(hGeo,atmosphereTable.u);
      connect(atmosphereTable.y[1],Temperature);
      connect(atmosphereTable.y[2],Pressure);
      // TODO should average of a/b be used?
      annotation(Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2}), graphics = {Text(origin = {0.22,-1.1}, extent = {{-96.027,93.8195},{96.91,-96.91}}, textString = "atmosphere"),Rectangle(origin = {-0.441501,0.662252}, extent = {{-95.8057,96.468},{95.8057,-96.468}})}), experiment(StartTime = 0, StopTime = 1000, Tolerance = 0.000001));
    end Atmosphere;
    annotation(Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})));
  end Aerodynamics;
  package Propulsion
    model Constant
      extends Modelica.Mechanics.MultiBody.Interfaces.PartialOneFrame_b;
      parameter Real torque(start = 0, fixed = true);
      parameter Real force(start = 0, fixed = true);
      Modelica.Mechanics.MultiBody.Forces.WorldForceAndTorque forceAndTorque(resolveInFrame = Modelica.Mechanics.MultiBody.Types.ResolveInFrameB.frame_resolve) annotation(Placement(visible = true, transformation(origin = {-25.6071,-0.441501}, extent = {{-10,-10},{10,10}}, rotation = 0)));
    equation
      forceAndTorque.force = {force,0,0};
      forceAndTorque.torque = {torque,0,0};
      connect(forceAndTorque.frame_b,forceAndTorque.frame_resolve) annotation(Line(points = {{-15.6071,-0.441501},{-14.128,-0.441501},{-14.128,18.1015},{-26.4901,18.1015},{-26.4901,10.596},{-26.4901,10.596}}));
      connect(forceAndTorque.frame_b,frame_b) annotation(Line(points = {{-15.6071,-0.441501},{8.83002,-0.441501},{8.83002,-0.441501},{8.83002,-0.441501}}));
      annotation(Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {1,1}), graphics = {Text(origin = {8.83353,-5.96302}, extent = {{-90.95,89.85},{62.6939,-1.54978}}, textString = "constant"),Text(origin = {0.223002,-49.0045}, extent = {{-92.49,41.5},{92.49,-41.5}}, textString = "propulsion"),Rectangle(origin = {32.6711,5.96026}, extent = {{-128.918,89.404},{62.6932,-102.649}})}));
    end Constant;
    annotation(Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})));
  end Propulsion;
  package Aircraft
    model Simple
      annotation(Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})));
    end Simple;
    annotation(Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})));
  end Aircraft;
  package World
    model AeroWorld
      //
      // Imports
      //
      import SI = Modelica.SIunits;
      import Modelica.Mechanics.MultiBody.*;
      //
      // Inheritance
      //
      extends Modelica.Icons.MaterialProperty;
      //
      // Outer
      //
      outer Modelica.Mechanics.MultiBody.World world;
      //
      // Parameters
      //
      parameter SI.Distance a = 6378137.0 "semi-major axis";
      parameter SI.Distance b = 6356752.0 "semi-minor axis";
      parameter SI.AngularVelocity omega = 0.00007292115 "World rotation rate w.r.t. inertial frame";
      parameter Real GMR = 34.163196 "hydrostatic constant";
      parameter Real R = 8.31451 "gas constant";
      parameter Real M_air = 0.0289645 "mean molar mass of air, in kg";
      parameter SI.Density rho_0 = 1.225 "density at sea level";
      parameter SI.Pressure P_0 = 101325 "pressure at sea level";
      parameter SI.Velocity windNav[3] = {0,0,0} "wind velocity in the navigation frame";
      //
      // Computed Constants
      //
      Real e = (a ^ 2 - b ^ 2) ^ 0.5 / a "eccentricity";
      //
      // Variables
      //
      SI.Angle phi(start = 0, fixed = true) "angle of rotation of world w.r.t. inertial frame about axis";
      //
      // Interfaces
      //
      Interfaces.Frame_b frame_wci "Coordinate system fixed in the origin of the world, not rotating with respect to inertial frame" annotation(Placement(visible = true, transformation(origin = {100,50}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      Interfaces.Frame_b frame_wcwf "Coordinate system fixed in the origin of the world, rotating with the world" annotation(Placement(visible = true, transformation(origin = {100,-50}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      //
      // Public functions with class parameters as defaults
      //
      function wcwfToGeodetic = OpenFDM.Util.wcwfToGeodetic(a = a, e = e);
      function geodeticToWcwf = OpenFDM.Util.geodeticToWcwf(a = a, e = e);
    equation
      //
      // Equations
      //
      connect(world.frame_b,frame_wci);
      Connections.branch(frame_wci, frame_wcwf);
      frame_wci.r_0 = frame_wcwf.r_0;
      frame_wcwf.R = Frames.axisRotation(3, phi, omega);
      der(phi) = omega;
      //
      // Annotations
      //
      annotation(defaultComponentName = "aeroworld", defaultComponentPrefixes = "inner", missingInnerMessage = "No \"aeroworld\" component is defined.", Documentation(info = "<HTML>
<p>
Model <b>Atmosphere</b> represents a global world with an atmoshere.
</p>
</html>"), Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2}), graphics = {Text(origin = {-8.03,118.29}, lineColor = {0,0,255}, extent = {{-100.89,26.34},{100.89,-26.34}}, textString = "%name")}), experiment(StartTime = 0, StopTime = 7680, Tolerance = 0.000001));
    end AeroWorld;
    annotation(Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})));
  end World;
  package Examples
    model Test "Two point masses in a point gravity field"
      Modelica.Mechanics.MultiBody.Forces.WorldForceAndTorque forceAndTorque annotation(Placement(visible = true, transformation(origin = {-17.2185,-7.06402}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      Modelica.Blocks.Sources.Constant force[3](k = {0,0,0}) annotation(Placement(visible = true, transformation(origin = {-59.4702,-29.0066}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      inner Modelica.Mechanics.MultiBody.World world(mue = 1, gravitySphereDiameter = 0.1, gravityType = Modelica.Mechanics.MultiBody.Types.GravityTypes.PointGravity) annotation(Placement(visible = true, transformation(origin = {-47.0861,-77.9912}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      Modelica.Blocks.Routing.Multiplex3 multiplex31 annotation(Placement(visible = true, transformation(origin = {71.9647,64.4592}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      Modelica.Blocks.Sources.IntegerConstant integerconstant1(k = 1) annotation(Placement(visible = true, transformation(origin = {-25.6071,43.7086}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      Modelica.Mechanics.MultiBody.Parts.FixedTranslation fixedtranslation1 annotation(Placement(visible = true, transformation(origin = {18.9845,-6.18102}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      Modelica.Blocks.Tables.CombiTable1Ds combitable1ds1(table = {{0,1},{0.001,0.001}}) annotation(Placement(visible = true, transformation(origin = {36.2031,63.5762}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      Modelica.Mechanics.MultiBody.Parts.BodyShape bodyshape1(r = {0,0,0}, r_CM = {0,0,0}, m = 1.0, sequence_angleStates = {3,2,1}) annotation(Placement(visible = true, transformation(origin = {79.4702,-7.50552}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      Modelica.Blocks.Routing.Extractor extractor1(nin = 3) annotation(Placement(visible = true, transformation(origin = {-0.441501,73.2892}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      Modelica.Mechanics.MultiBody.Sensors.RelativeAngles relativeangles1(sequence = {3,2,1}) annotation(Placement(visible = true, transformation(origin = {18.1015,-48.1236}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      OpenFDM.Aerodynamics.AeroSensor aerosensor1 annotation(Placement(visible = true, transformation(origin = {-53.4216,30.0221}, extent = {{-10,-10},{10,10}}, rotation = 0)));
    equation
      connect(relativeangles1.frame_a,world.frame_b) annotation(Line(points = {{8.1015,-48.1236},{-11.9205,-48.1236},{-11.9205,-77.7042},{-27.3731,-77.7042},{-37.0861,-78.4327},{-37.0861,-77.9912}}));
      connect(relativeangles1.frame_b,bodyshape1.frame_a) annotation(Line(points = {{28.1015,-48.1236},{68.4327,-48.1236},{69.4702,-7.94702},{69.4702,-7.50552}}));
      connect(relativeangles1.angles,extractor1.u) annotation(Line(points = {{18.1015,-59.1236},{-83.0022,-59.1236},{-83.0022,73.2892},{-18.6225,73.2892},{-12.4415,73.2892}}));
      connect(extractor1.y,combitable1ds1.u) annotation(Line(points = {{10.5585,73.2892},{15.011,73.2892},{15.011,63.1347},{24.6446,63.5762},{24.2031,63.5762}}));
      connect(integerconstant1.y,extractor1.index) annotation(Line(points = {{-14.6071,43.7086},{-0.883002,43.7086},{-0.441501,61.2892},{-0.441501,61.2892}}));
      connect(fixedtranslation1.frame_b,bodyshape1.frame_a) annotation(Line(points = {{28.9845,-7.06402},{69.7572,-7.06402},{69.7572,-6.62252},{70.1987,-7.50552},{69.4702,-7.50552}}));
      connect(combitable1ds1.y,multiplex31.u1) annotation(Line(points = {{47.2031,63.5762},{59.9647,71.4592}}));
      connect(multiplex31.u2,combitable1ds1.y) annotation(Line(points = {{59.9647,64.4592},{47.2031,63.5762}}));
      connect(multiplex31.u3,combitable1ds1.y) annotation(Line(points = {{59.9647,57.4592},{47.2031,63.5762}}));
      connect(fixedtranslation1.frame_a,forceAndTorque.frame_b) annotation(Line(points = {{8.98455,-7.06402},{-6.18102,-7.06402},{-6.18102,-6.18102},{-6.18102,-6.18102}}));
      connect(multiplex31.y,forceAndTorque.torque) annotation(Line(points = {{82.9647,64.4592},{91.8322,64.4592},{91.8322,12.362},{-38.8521,12.362},{-38.8521,-1.3245},{-29.2185,-1.3245},{-29.2185,-1.06402}}));
      connect(force.y,forceAndTorque.force) annotation(Line(points = {{-48.4702,-29.0066},{-41.0596,-29.0066},{-41.0596,-13.245},{-30.4636,-13.245},{-30.4636,-13.245}}));
      connect(forceAndTorque.frame_resolve,forceAndTorque.frame_b) annotation(Line(points = {{-17.2185,2.93598},{-7.50552,2.93598},{-7.50552,-8.38852},{-7.50552,-8.38852}}));
    end Test;
    model TestWind
      //
      // Imports
      //
      import SI = Modelica.SIunits;
      //
      // Parameters
      //
      parameter SI.Angle latitude_start = 0.1;
      parameter SI.Angle longitude_start = 0;
      parameter SI.Distance asl_start = 100;
      parameter SI.Angle roll_start = 0;
      parameter SI.Angle pitch_start = 0;
      parameter SI.Angle yaw_start = 0;
      //
      // Computed Variables
      //
      SI.Distance r_0_start[3] = aeroworld.geodeticToWcwf(latitude_start, longitude_start, asl_start);
      //
      // Inner
      //
      inner OpenFDM.World.AeroWorld aeroworld annotation(Placement(visible = true, transformation(origin = {-83.2079,-33.5099}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      inner ModelicaServices.Modelica3D.Controller m3d_control(framerate = 15) annotation(Placement(visible = true, transformation(origin = {-49.0066,-64.9007}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      inner Modelica.Mechanics.MultiBody.World world(mue = 398600441800000.0, gravitySphereDiameter = 12756200, gravityType = Modelica.Mechanics.MultiBody.Types.GravityTypes.PointGravity, enableAnimation = false, animateWorld = true, animateGravity = true, defaultN_to_m = 10, defaultNm_to_m = 0.001) annotation(Placement(visible = true, transformation(origin = {-82.3249,-64.415}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      //
      // Components
      //
      Modelica.Mechanics.MultiBody.Parts.BodyShape body(angles_fixed = true, angles_start = {1.57,0,-1.57}, w_0_fixed = true, sequence_start = {3,2,1}, m = 1.0, r = {-1,0,0}, r_CM = {0,0,0}, r_shape = {0,0,0}, width = 0.05, height = 0.05, sphereDiameter = 0.1, I_11 = 0.01, I_22 = 0.01, I_33 = 0.01, z_0_fixed = false, sequence_angleStates = {3,2,1}) annotation(Placement(visible = true, transformation(origin = {70.2741,54.5284}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      Modelica.Mechanics.MultiBody.Parts.FixedRotation fixedrotation1(n = {0,1,0}, angle = 0, r = {-1,0,0}, sequence = {3,2,1}) annotation(Placement(visible = true, transformation(origin = {-23.8411,25.6071}, extent = {{10,-10},{-10,10}}, rotation = 0)));
      Modelica.Mechanics.MultiBody.Parts.FixedRotation fixedrotation2(r = {0,0,0}, n = {0,1,0}, angle = 0) annotation(Placement(visible = true, transformation(origin = {-29.1391,55.1876}, extent = {{10,-10},{-10,10}}, rotation = 0)));
      OpenFDM.Aerodynamics.Simple simpleAero(cDcLSq = 0.01, cD0 = 0.01, cL0 = 0.1, cmq = -0.1) annotation(Placement(visible = true, transformation(origin = {-77.7042,53.8631}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      OpenFDM.Propulsion.Constant constantPropulsion(force(start = 0.2, fixed = false), torque = 0) annotation(Placement(visible = true, transformation(origin = {-78.5872,25.6071}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      OpenFDM.Frames.NAV nav annotation(Placement(visible = true, transformation(origin = {18.4685,-6.75676}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      //
      // Equations
      //
    equation
      connect(nav.frame_b,body.frame_a) annotation(Line(points = {{28.4685,-6.75676},{61.2613,-6.75676},{61.2613,54.0541},{61.2613,54.0541}}));
      connect(fixedrotation1.frame_b,constantPropulsion.frame_b) annotation(Line(points = {{-33.8411,25.6071},{-67.9912,25.6071},{-68.5872,23.8411},{-68.5872,25.6071}}));
      connect(fixedrotation2.frame_b,simpleAero.frame_b) annotation(Line(points = {{-39.1391,55.1876},{-67.5497,55.1876},{-67.7042,55.1876},{-67.7042,53.8631}}));
      connect(fixedrotation2.frame_a,body.frame_a) annotation(Line(points = {{-19.1391,55.1876},{60.0442,55.1876},{60.0442,54.5284},{60.2741,54.5284}}));
      connect(body.frame_a,fixedrotation1.frame_a) annotation(Line(points = {{60.2741,54.5284},{4.85651,54.5284},{4.85651,25.6071},{-13.245,25.6071},{-13.245,25.6071}}));
      //
      // Initial conditions
      //
    initial equation
      body.r_0 = r_0_start;
      nav.relativesensor.a_rel[1] = 0;
      nav.relativesensor.v_rel = {50,0,0};
      //
      // Annotations
      //
      annotation(Placement(transformation(extent = {{-80,-20},{-60,0}}, rotation = 0)), Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), experiment(StartTime = 0, StopTime = 10, Tolerance = 0.000001));
    end TestWind;
    model GravTest "Two point masses in a point gravity field"
      extends Modelica.Icons.Example;
      Modelica.Mechanics.MultiBody.Parts.Body body1(m = 1, sphereDiameter = 0.2, I_11 = 0.1, I_22 = 0.1, I_33 = 0.1, r_0(start = {8000,1,1}, each fixed = true), v_0(start = {1,0,0}, each fixed = true), angles_fixed = true, w_0_fixed = true, r_CM = {0,0,0}) annotation(Placement(transformation(extent = {{-20,20},{0,40}}, rotation = 0)));
      Modelica.Mechanics.MultiBody.Parts.Body body2(m = 1, sphereDiameter = 0.1, I_11 = 0.1, I_22 = 0.1, I_33 = 0.1, r_0(start = {0.6,0.6,0}, each fixed = true), v_0(start = {0.6,0,0}, each fixed = true), angles_fixed = true, w_0_fixed = true, r_CM = {0,0,0}) annotation(Placement(visible = true, transformation(origin = {30,29.117}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      inner ModelicaServices.Modelica3D.Controller m3d_control annotation(Placement(visible = true, transformation(origin = {-75.9382,58.7196}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      inner Modelica.Mechanics.MultiBody.World world(mue = 1, gravitySphereDiameter = 0.1, gravityType = Modelica.Mechanics.MultiBody.Types.GravityTypes.PointGravity) annotation(Placement(visible = true, transformation(origin = {-74.1509,-64.2453}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      annotation(experiment(StopTime = 5), Documentation(info = "<HTML>
<p>
This model demonstrates a point gravity field. Two bodies
are placed in the gravity field. The initial positions and velocities of
these bodies are selected such that one body rotates on a circle and
the other body rotates on an ellipse around the center of the
point gravity field.
</p>

<IMG src=\"modelica://Modelica/Resources/Images/Mechanics/MultiBody/Examples/Elementary/PointGravity.png\"
ALT=\"model Examples.Elementary.PointGravity\">
</HTML>"));
    end GravTest;
    model TestShapes
      inner Modelica.Mechanics.MultiBody.World world(g = 0, animateWorld = true, animateGravity = false) annotation(Placement(visible = true, transformation(origin = {-78.3471,-79.0083}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      inner ModelicaServices.Modelica3D.Controller m3d_control annotation(Placement(visible = true, transformation(origin = {-74.0496,80.6612}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      Modelica.Mechanics.MultiBody.Parts.Body body1 annotation(Placement(visible = true, transformation(origin = {-49.1647,27.2076}, extent = {{-10,-10},{10,10}}, rotation = 0)));
      annotation(Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})));
    end TestShapes;
    annotation(Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})));
  end Examples;
  package Frames
    model WCI
      outer OpenFDM.World.AeroWorld aeroworld;
      import SI = Modelica.SIunits;
      Modelica.Mechanics.MultiBody.Interfaces.Frame_b frame_b "Coordinate system fixed in the origin of the world, rotating with the world" annotation(Placement(transformation(extent = {{84,-16},{116,16}}, rotation = 0)));
    equation
      connect(frame_b,aeroworld.frame_wci);
      annotation(Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2}), graphics = {Rectangle(origin = {-0.883002,2.64901}, extent = {{-95.3642,94.9227},{96.2472,-98.4547}}),Text(origin = {2.65,-1.99}, extent = {{-86.09,49.23},{86.09,-49.23}}, textString = "WCI")}));
    end WCI;
    model WCWF
      outer OpenFDM.World.AeroWorld aeroworld;
      import SI = Modelica.SIunits;
      Modelica.Mechanics.MultiBody.Interfaces.Frame_b frame_b "Coordinate system fixed in the origin of the world, rotating with the world" annotation(Placement(transformation(extent = {{84,-16},{116,16}}, rotation = 0)));
    equation
      connect(frame_b,aeroworld.frame_wcwf);
      annotation(Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2}), graphics = {Rectangle(origin = {-0.883002,2.64901}, extent = {{-95.3642,94.9227},{96.2472,-98.4547}}),Text(origin = {2.65,-1.99}, extent = {{-86.09,49.23},{86.09,-49.23}}, textString = "WCWF")}));
    end WCWF;
    model NAV
      import Modelica.Mechanics.MultiBody.*;
      outer OpenFDM.World.AeroWorld aeroworld;
      import SI = Modelica.SIunits;
      extends Modelica.Mechanics.MultiBody.Interfaces.PartialOneFrame_b;
      Modelica.Blocks.Interfaces.RealOutput latitude(final quantity = "Angle", final unit = "rad", displayUnit = "deg") "latitude" annotation(Placement(transformation(origin = {-100,-110}, extent = {{-10,-10},{10,10}}, rotation = 270)));
      Modelica.Blocks.Interfaces.RealOutput longitude(final quantity = "Angle", final unit = "rad", displayUnit = "deg") "longitude" annotation(Placement(transformation(origin = {-50,-110}, extent = {{-10,-10},{10,10}}, rotation = 270)));
      Modelica.Blocks.Interfaces.RealOutput asl(final quantity = "Distance", final unit = "m") "altitude above sea level" annotation(Placement(transformation(origin = {0,-110}, extent = {{-10,-10},{10,10}}, rotation = 270)));
      Modelica.Blocks.Interfaces.RealOutput euler[3](each final quantity = "Angle", each final unit = "rad") "3-2-1 euler angles" annotation(Placement(transformation(origin = {50,-110}, extent = {{-10,-10},{10,10}}, rotation = 270)));
      Modelica.Mechanics.MultiBody.Sensors.RelativeSensor relativesensor(get_a_rel = true, get_r_rel = true, get_w_rel = false, get_v_rel = true, get_angles = true, sequence = {3,2,1}, resolveInFrameAfterDifferentiation = Modelica.Mechanics.MultiBody.Types.ResolveInFrameAB.frame_b, animation = false) annotation(Placement(visible = true, transformation(origin = {-15.894,-9.71302}, extent = {{-10,-10},{10,10}}, rotation = 0)));
    equation
      connect(relativesensor.frame_a,aeroworld.frame_wcwf);
      connect(relativesensor.frame_b,frame_b);
      (latitude,longitude,asl) = aeroworld.wcwfToGeodetic(relativesensor.r_rel);
      connect(relativesensor.angles,euler);
      annotation(Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2}), graphics = {Rectangle(origin = {-0.883002,2.64901}, extent = {{-95.3642,94.9227},{96.2472,-98.4547}}),Text(origin = {2.65,-1.99}, extent = {{-86.09,49.23},{86.09,-49.23}}, textString = "NAV")}));
    end NAV;
    model WIND
      outer OpenFDM.World.AeroWorld aeroworld;
      import SI = Modelica.SIunits;
      Modelica.Mechanics.MultiBody.Interfaces.Frame_b frame_b "Coordinate system fixed in the origin of the world, rotating with the world" annotation(Placement(transformation(extent = {{84,-16},{116,16}}, rotation = 0)));
    equation
      connect(frame_b,aeroworld.frame_wci);
      annotation(Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2}), graphics = {Rectangle(origin = {-0.883002,2.64901}, extent = {{-95.3642,94.9227},{96.2472,-98.4547}}),Text(origin = {2.65,-1.99}, extent = {{-86.09,49.23},{86.09,-49.23}}, textString = "WIND")}));
    end WIND;
    annotation(Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})));
  end Frames;
  package Util
    function wcwfToGeodetic
      import SI = Modelica.SIunits;
      input SI.Position r[3] "Position vector from world frame to actual point, resolved in world frame";
      input SI.Distance a "semi-major axis";
      input Real e "semi-major axis";
      input SI.Distance hTol = 0.000001 "altitude tolerance";
      input Integer hIterMax = 10 "maximum WCWF-> geodetic altitude iterations";
      output SI.Angle latitude "Latitude";
      output SI.Angle longitude "Longitude";
      output SI.Distance asl "Altitude above sea level";
    protected
      SI.Distance N,aslOld,rxy;
      Integer loopCount;
    algorithm
      longitude:=atan2(r[2], r[1]);
      rxy:=(r[1] ^ 2 + r[2] ^ 2) ^ 0.5;
      N:=a;
      asl:=0;
      aslOld:=2 * hTol;
      loopCount:=0;
      while (asl - aslOld < hTol and loopCount < 10) loop
              latitude:=atan(r[3] / (rxy * (1 - N * e ^ 2 / (N + asl))));
        N:=a / (1 - e ^ 2 * sin(latitude) ^ 2) ^ 0.5;
        aslOld:=asl;
        asl:=rxy / cos(latitude) - N;
        loopCount:=loopCount + 1;
      end while;
    end wcwfToGeodetic;
    function cartesianToSpherical
      import SI = Modelica.SIunits;
      input SI.Distance r[3] "cartesian coordinates";
      output SI.Angle alpha "inclination angle";
      output SI.Angle beta "declination angle";
      output Real length "length";
    algorithm
      length:=sqrt(r[1] ^ 2 + r[2] ^ 2 + r[3] ^ 2);
      alpha:=atan(r[3] / sqrt(r[1] ^ 2 + r[2] ^ 2));
      beta:=atan(r[2] / r[1]);
      annotation(Inline = true);
    end cartesianToSpherical;
    function geodeticToWcwf
      import SI = Modelica.SIunits;
      import Modelica.Utilities.Streams;
      input SI.Angle latitude "Latitude";
      input SI.Angle longitude "Longitude";
      input SI.Distance asl "Altitude above sea level";
      input SI.Distance a "semi-major axis";
      input Real e "semi-major axis";
      output SI.Position r[3] "Position vector from world frame to actual point, resolved in world frame";
    protected
      SI.Distance N "prime vertical radius of curvature";
    algorithm
      N:=a / sqrt(1 - e ^ 2 * sin(latitude) ^ 2);
      r:={(N + asl) * cos(latitude) * cos(longitude),(N + asl) * cos(latitude) * sin(longitude),(N * (1 - e ^ 2) + asl) * sin(latitude)};
      annotation(Inline = true);
    end geodeticToWcwf;
  end Util;
  annotation(Icon(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})), Diagram(coordinateSystem(extent = {{-100,-100},{100,100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2,2})));
end OpenFDM;

